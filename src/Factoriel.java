class Factoriel {
    static int factorielle(int h) {
        int a = 1;
        for (int i = 1 ; i <= h ; i++)
        {
            a *= i;
        }
        return a;
    }
}

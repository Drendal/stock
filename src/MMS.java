public class MMS {
    private double lambda;//taux de naissance
    private double mu;//taux de mort
    private int s;//nombre de serveur

    private double q;//probabilité d'être dans un état donné
    private double l;//moyenne de client dans le système
    private double w;//temps moyen dans le système
    private double lq;//moyenne de client dans l'état
    private double wq;//temps moyen dans la file
/*
    public MMS() {
    }*/

    MMS(double lambda, double mu, int s) throws IllegalArgumentException, ArithmeticException {
        if (s <= 0 || mu <= 0 || lambda <= 0) {
            throw new IllegalArgumentException("Erreur : le nombre de serveur et les taux de naissance et de mort doivent être strictement positif.");
        }

        if (lambda/(s*mu) >= 1) {
            throw new ArithmeticException("Blocage : Rho supérieur à 1.");
        }

        this.lambda = lambda;
        this.mu = mu;
        this.s = s;
    }

    void calculation() {

        double somme = 0;
        for (int j = 0 ; j < s ; j++)
        {
            somme += Math.pow((lambda/mu),j)/ Factoriel.factorielle(j);
        }

        double tmp = Math.pow(lambda/(s*mu),2)*s;
        tmp = Math.pow(tmp,s);
        tmp /= Factoriel.factorielle(s)*(1-(lambda/(s*mu)));

        somme += tmp;

        q = 1/somme;

        lq = q * Math.pow((lambda/mu),2);
        lq *= (lambda/(s*mu));
        lq /= Factoriel.factorielle(s)*Math.pow(1-(lambda/(s*mu)),2);

        wq = lq/lambda;

        w = wq + 1 / mu;

        l = lambda * w;
    }

    public String toString() {
        return "lambda : " + lambda + "\nmu : " + mu + "\ns : " + s + "\n\nl : " + l + "\nw : " + w + "\nq : " + q + "\nlq : " + lq + "\nwq : " + wq + "\n";
    }

    /**
     * Durée de séjour dans le système
     * @param t temps
     * @return la probabilité Tau > t
     */
    double durSysteme(double t) throws IllegalArgumentException {
        if (t < 0) {
            throw new IllegalArgumentException("Erreur : t doit être positif.");
        }

        if (s-1-(lambda/mu) == 0) {
            throw new ArithmeticException("Erreur : division par 0 au niveau de la durée de séjour système");
        }

        double frac = Math.exp(-mu*t*(s-1-(lambda/mu)));
        frac = 1-frac;
        frac /= s-1-(lambda/mu);

        double mult = 1 / (Factoriel.factorielle(s)*(1-(lambda/(s*mu))));
        mult *= q * Math.pow((lambda/mu),s);

        return (1 + frac * mult) * Math.exp(-mu*t);
    }

    /**
     * Durée d'attente pour service
     * @param t temps
     * @return la probabilité Tau > t
     */
    public double durService(double t) {
        double p = q * Math.pow((lambda/mu),2) / (Factoriel.factorielle(s)*(1-(lambda/(s*mu))));

        return Math.exp(-s*mu*t*(1-lambda/(s*mu))) * p;
    }

    public double getLambda() {
        return lambda;
    }

    public double getMu() {
        return mu;
    }

    public int getS() {
        return s;
    }

    public double getQ() {
        return q;
    }

    public double getL() {
        return l;
    }

    public double getW() {
        return w;
    }

    public double getLq() {
        return lq;
    }

    public double getWq() {
        return wq;
    }
}
